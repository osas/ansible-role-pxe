# Ansible role for PXE Data installation

## Introduction

This role installs and configure the server. Setting up the firewall is not done yet (but planned).

This role comes with various distributions already defined.

## Variables

- **tftp_dir**: TFTP data directory (defaults to /var/lib/tftpboot)
- **global_append**: kernel append parameters for all menu entries
- **unattended_install**: global parameters (in a hash) for unattended installations:
    - **data**: parameters about data storage (automatic installation files):
         - **node**: ansible node name where to generate the files
         - **path**: directory in which to generate the files
    - **secrets**: parameters about secrets storage (passwords):
         - **node**: ansible node name where to generate the files
         - **path**: directory in which to generate the files
- **distributions**: list of distributions, grouped by family (in a hash), and versions (is a sub-hash), defined by these parameters:
    - **base_version**: optional version on which this variant is based
    - **comment**: additional comment to define the release or this specific PXE entry (defaults to empty)
    - **architectures**: list of architectures of the release (as defined by the distribution vendor)
    - **append**: kernel append parameters for this distribution
    - **unattended_install**: either a list of parameters (in a hash) used to generate a configuration file to automate the installation
                                The file is created using the `unattended_install_config` role using these parameters
                              or the relative path to an already generated configuration file
                                The path is relative to `unattended_install.data.path` and must be generated and installed outside this role.
- **distributions_families**: list of distributions families, each is a hash with these parameters:
    - **default_mirror**: base URL for the installation
    - **pxe_path**: path added to the base URL to find the PXE files
    - **kernel**: kernel filename
    - **initrd**: initrd filename
    - **append**: kernel append parameters for this family

The repository and pxe URLs are computed automatically from the `default_mirror` for major distributions. It is necessary since it may change based on the version. If the repository is custom the `default_mirror` may be an URL containing `_ARCH_` and `_VER_` special strings, which will be replaced respectively by the distribution `architecture` and `version`.

The append parameters are concatenated using the global, directibution family, and distribution parameters. These append fragments may contain `_URL_`, `_ARCH_` and `_VER_` special strings, which will be replaced respectively by the repository `url`, distribution `architecture` and `version`.

## Variants

If you need to define a different installation based on the same distribution, then you need to specify the `base_version` parameter. it will appear as a specific PXE menu entry.

Variants share the same PXE files (kernel and initrd) and `_VER_` is set to the base version. What changes are the `comment` and `append` parameters, as well as the list of supported architectures.

## Distribution repository and PXE data URLs

These info can be computed by giving the wanted `family` and `version` and calling the `distro_urls` entrypoint using `include_role`. The resulting URLs can be found in the `repo_url` and `pxe_url` variables.

As these URLs also depend on the architecture, you can specify the wanted `architecture` too. If you don't, the URLs will contain `_ARCH_` when this information is needed, for you to replace in a later processing. This is used internally by this role to loop over the supported architectures.

